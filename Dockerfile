FROM mono:latest
WORKDIR /app

# Copy everything
COPY . ./
# Restore as distinct layers
RUN mcs -out:hello.exe myapp.cs

# Build and publish a release
RUN mono hello.exe

# Build runtime image
#FROM mcr.microsoft.com/dotnet/aspnet:6.0
#WORKDIR /app
#COPY --from=build-env /app/out .
#ENTRYPOINT ["dotnet", "MathOperations.dll"]
#  docker build -t math-operations .
#  docker run math-operations
