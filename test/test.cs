using System;
using Xunit;

namespace MathOperations.Tests
{
    public class ProgramTests
    {
        [Fact]
        public void Add_GivenTwoNumbers_ReturnsCorrectSum()
        {
            // Arrange
            int a = 5;
            int b = 3;

            // Act
            int result = Program.Add(a, b);

            // Assert
            Assert.Equal(8, result);
        }

        [Fact]
        public void Multiply_GivenTwoNumbers_ReturnsCorrectProduct()
        {
            // Arrange
            int a = 7;
            int b = 4;

            // Act
            int result = Program.Multiply(a, b);

            // Assert
            Assert.Equal(28, result);
        }

        [Fact]
        public void Divide_GivenTwoNumbers_ReturnsCorrectQuotient()
        {
            // Arrange
            int a = 15;
            int b = 3;

            // Act
            int result = Program.Divide(a, b);

            // Assert
            Assert.Equal(5, result);
        }
    }
}