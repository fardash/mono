using System;

namespace MathOperations
{
    public class Program
    {
        public static void Main(string[] args)
        {
            int a = 10;
            int b = 5;

            Console.WriteLine($"Addition: {Add(a, b)}");
            Console.WriteLine($"Multiplication: {Multiply(a, b)}");
            Console.WriteLine($"Division: {Divide(a, b)}");
        }

        public static int Add(int x, int y)
        {
            return x + y;
        }

        public static int Multiply(int x, int y)
        {
            return x * y;
        }

        public static int Divide(int x, int y)
        {
            return x / y;
        }
    }
}